<?php

/**
 * @file
 * Provides a controller building upon the EntityAPI controller but providing
 * features for handling entities located in remote databases.
 */

/**
 * A controller providing all features EntityAPI controller does, but with support
 * for entities in remote databases.
 */
class RemoteEntityController extends EntityAPIController {
  
  /**
   * Key of the database containing IB Company entities.
   *
   * @var string
   */
  protected $targetKey;
  
  /**
   * Connection to the database containing IB Company entities.
   *
   * @var DatabaseConnection
   */
  protected $connection;

  /**
   * Overridden.
   * @see DrupalDefaultEntityController#__construct()
   * @see EntityAPIController#__construct()
   *
   * Allows specifying a 'base database' key which refers to a database key specified
   * in the settings.php file. This will be the database where the entity data is stored.
   */
  public function __construct($entityType) {
    parent::__construct($entityType);
    
    $this->targetKey = isset($this->entityInfo['base database'])? $this->entityInfo['base database'] : 'default';
    $this->connection = Database::getConnection('default', $this->targetKey);
  }
  
  /**
   * Overridden.
   * @see DrupalDefaultEntityController#buildQuery()
   * @see EntityAPIController#buildQuery()
   *
   * Builds a query using the remote database connection for loading entity data.
   */
  protected function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {
    $previousdb = db_set_active($this->targetKey);
    $query = parent::buildQuery($ids, $conditions, $revision_id); 
    db_set_active($previousdb);
    return $query;
  }
  
  /**
   * Overridden
   * @see EntityAPIController#delete()
   *
   * @param $transaction
   *   Optionally a DatabaseTransaction object to use. Allows overrides to pass
   *   in their transaction object.
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $entities = $ids ? $this->load($ids) : FALSE;
    if (!$entities) {
      // Do nothing, in case invalid or no ids have been passed.
      return;
    }
    $transaction = isset($transaction) ? $transaction : $this->connection->startTransaction();

    try {
      $this->connection
        ->delete($this->entityInfo['base table'])
        ->condition($this->nameKey, array_keys($entities), 'IN')
        ->execute();
      // Reset the cache as soon as the changes have been applied.
      $this->resetCache($ids);

      foreach ($entities as $id => $entity) {
        $this->invoke('delete', $entity);
        if (entity_has_status($this->entityType, $entity, ENTITY_IN_CODE)) {
          $rebuild = TRUE;
        }
      }
      // Ignore slave server temporarily, unless data is located in a remote database.
      if (!isset($this->targetKey) || ($this->targetKey == 'default')) {
        db_ignore_slave();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception($this->entityType, $e);
      throw $e;
    }
    // We have to postpone rebuilding defaults until the transaction has been
    // committed. This is as variable_set() triggers a merge query and dies with
    // a savepoint exception if it is inside a transaction.
    if (!empty($rebuild)) {
      $transaction = NULL;
      entity_defaults_rebuild(array($this->entityType));
    }
  }

  /**
   * Overridden
   * @see EntityAPIController#save()
   *
   * @param $transaction
   *   Optionally a DatabaseTransaction object to use. Allows overrides to pass
   *   in their transaction object.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $transaction = isset($transaction) ? $transaction : $this->connection->startTransaction();
    try {
      // Load the stored entity, if any.
      if (!empty($entity->{$this->nameKey}) && !isset($entity->original)) {
        // In order to properly work in case of name changes, load the original
        // entity using the id key if it is available.
        $entity->original = entity_load_unchanged($this->entityType, !empty($entity->{$this->idKey}) ? $entity->{$this->idKey} : $entity->{$this->nameKey});
      }
      // Update the status for entities getting overridden.
      if (entity_has_status($this->entityType, $entity, ENTITY_IN_CODE) && empty($entity->is_rebuild)) {
        $entity->{$this->statusKey} |= ENTITY_CUSTOM;
      }

      $this->invoke('presave', $entity);

      if (!empty($entity->{$this->idKey}) && empty($entity->is_new)) {
        // Note: A core patch is needed to make drupal_write_record write to a remote database
        // See: Issue #1125024
        $return = drupal_write_record($this->entityInfo['base table'], $entity, $this->idKey);
        $this->resetCache(array($entity->{$this->nameKey}));
        $this->invoke('update', $entity);
      }
      else {
        // Note: A core patch is needed to make drupal_write_record write to a remote database
        // See: Issue: #1125024
        $return = drupal_write_record($this->entityInfo['base table'], $entity);
        $this->invoke('insert', $entity);
      }
      // Ignore slave server temporarily, unless data is located in a remote database.
      if (!isset($this->targetKey) || ($this->targetKey == 'default')) {
        db_ignore_slave();
      }
      unset($entity->is_new);
      unset($entity->original);

      return $return;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception($this->entityType, $e);
      throw $e;
    }
  }
}
